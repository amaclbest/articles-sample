package cl.demos.articles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.demos.articles.entity.Article;

@Repository
public interface ArticleDao extends JpaRepository<Article, Integer> {
}