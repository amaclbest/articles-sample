package cl.demos.articles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.demos.articles.dao.ArticleDao;
import cl.demos.articles.entity.Article;

@Service
public class ArticleService {
	@Autowired
	ArticleDao articleDao;

	public List<Article> getAllArticles() {
		return this.articleDao.findAll();
	}

	public Article addArticle(Article article) {
		return this.articleDao.save(article);
	}
}