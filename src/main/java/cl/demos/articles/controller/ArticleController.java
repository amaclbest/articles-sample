package cl.demos.articles.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.demos.articles.entity.Article;
import cl.demos.articles.service.ArticleService;

@RestController
@RequestMapping("/article")
public class ArticleController {
	@Autowired
	ArticleService articleService;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Article> getAllUsers() {
		return articleService.getAllArticles();
	}

	@RequestMapping(value = "/addarticle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody()
	public Article addNewUser(@RequestBody Article article) {
		return this.articleService.addArticle(article);
	}

	// other controllers omitted for brevity

}
