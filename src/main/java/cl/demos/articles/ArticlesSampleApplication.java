package cl.demos.articles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticlesSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticlesSampleApplication.class, args);
	}

}
