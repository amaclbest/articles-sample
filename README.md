## Docker Oracle 11g

1. docker-compose para iniciar oracle 11g.

```dockerfile
version: '3'

services:
  oracle-db:
    image: oracleinanutshell/oracle-xe-11g:latest
    ports:
      - 1521:1521
      - 5500:5500
```
2. Credenciales de acceso a la DB
```
username: system
password: oracle
```

3. Script para cargar datos en la db.
```sql
CREATE TABLE article (
  id   NUMBER,
  title  VARCHAR2(30),
  content VARCHAR2(200),
  create_date date,
  publish_date date
);

SELECT * FROM article;

CREATE SEQUENCE article_id_sq
INCREMENT BY 1;

INSERT INTO article (id,title,content,create_date,publish_date) VALUES (article_id_sq.nextval,'title 1','content 1',current_date,current_date);
INSERT INTO article (id,title,content,create_date,publish_date) VALUES (article_id_sq.nextval,'title 2','content 2',current_date,current_date);
INSERT INTO article (id,title,content,create_date,publish_date) VALUES (article_id_sq.nextval,'title 3','content 3',current_date,current_date);
INSERT INTO article (id,title,content,create_date,publish_date) VALUES (article_id_sq.nextval,'title 4','content 4',current_date,current_date);
```
